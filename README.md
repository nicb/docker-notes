# DOCKER NOTES

This is a set of Docker notes and scripts I either wrote or stole or copied - or they were given to me.

## ACKNOWLEDGEMENTS

I wish to acknowledge Leonardo Sarra who helped me throughout with `dockers`,
`containers`, and `composition`.

## [LICENSE](./LICENSE)

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Docker-notes</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://git.smerm.org/nicb/Docker-notes" rel="dct:source">https://git.smerm.org/nicb/Docker-notes</a>.
[Full text license](./LICENSE)
